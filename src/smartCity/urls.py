"""smartCity URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from city.views import *
from django.conf.urls import include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^search/location/(?P<sType>[-\w]+)/(?P<sCity>[-\w]+)$', locationSearchView.as_view(), name = "locationSearch"),
    url(r'^search/location/(?P<sType>[-\w]+)/$', locationSearchView.as_view(), name = "locationSearch"),
    url(r'^search/location/', locationSearchView.as_view(), name = "locationSearch"),
    url(r'^location/(?P<slug>[-\w]+)/$', locationView.as_view(), name="location"),
    url(r'^search/user/(?P<searchUsername>[-\w]+)$', userSearchView.as_view(), name="userSearch"),
    url(r'^search/user/', userSearchView.as_view(), name="userSearch"),
    url(r'^user/(?P<searchUsername>[-\w]+)/$', userView.as_view(), name="user"),
    url(r'^register/$', registerView.as_view(), name = "register"),
    url(r'^logout/$', logoutView.as_view(), name = "logout"),
    url(r'^login/$', loginView.as_view(), name = "login"),
    url(r'^ticket/$', ticketView.as_view(), name = "ticket"),
    url(r'^login/oauth/', include('social_django.urls', namespace='social')),
    url(r'^index/$', indexView.as_view(), name = "index"),
    url(r'^exportUsers/$', exportUserView.as_view(), name = 'exportUsers'),  
    url(r'^exportLocations/$', exportLocationView.as_view(), name = 'exportLocations'),
    url(r'^$', indexView.as_view())
]

LOGIN_URL = 'login'
LOGOUT_URL = 'logout'
LOGIN_REDIRECT_URL = 'index'

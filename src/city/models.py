from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator

from django.db.models.signals import post_save


#############################
#       CityUser
#############################
# 
# Fields:
#   user (Auth User)
#   phone_number
#   occupation
#   language
#
#############################
class cityUser(models.Model):

    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE
    )

    phone_number = models.CharField(
        max_length = 10, 
        validators = [MinLengthValidator(8)],
        default = "0411122233"
    ) 

    occupation_choices = (
        ("TR", "Tourist"),
        ("ST", "Student"),
        ("BN", "Business")
        )
    
    language_choices = (
        ("EN", "English"),
        ("US", "American English")
    )

    occupation = models.CharField(
        max_length = 2,
        choices = occupation_choices,
        default = "ST")
    
    language = models.CharField(
        max_length = 2,
        choices = language_choices,
        default = "EN"
    )

    def __str__(self):
        return self.user.username


def create_user(sender, **kwargs):
    if kwargs['created']:
        city_Profile = cityUser.objects.create(user = kwargs['instance'])

post_save.connect(create_user, sender = User)

#############################
#       cityLocation
#############################
# 
# Fields:
#   slug
#   location_type
#   city
#   name
#   address
#   phone_number
#   industry_type
#   departments
#
#############################
class cityLocation(models.Model):

    slug = models.SlugField(
        unique = True,
        max_length = 64
        )

    city_choices = (
        ("BNE", "Brisbane"),
        ("SYD", "Sydney"),
        ("MEL", "Melbourne")
    )

    location_choices = (
        ("CL", "College"),
        ("LB", "Library"),
        ("ID", "Industry"),
        ("HT", "Hotel"),
        ("PK", "Park"),
        ("ZO", "Zoo"),
        ("MU", "Museum"),
        ("RT", "Restaurant"),
        ("ML", "Mall")
    )

    location_type = models.CharField(
        max_length = 2,
        choices = location_choices,
        default = "PK"
    )

    city = models.CharField(
        max_length = 3,
        choices = city_choices,
        default = "BNE"
    )

    name = models.CharField(
        max_length = 128
    )

    address = models.CharField(
        max_length = 128
    )

    phone_number = models.CharField(
        max_length = 8, 
        validators = [MinLengthValidator(8)],
        default = "00000000"
    )

    departments = models.TextField(
        blank = True,
        null = True
    )

    industry_type = models.CharField(
        blank = True,
        null = True,
        max_length = 128
    )

    def __str__(self):
        return self.name + " ( Phone: " + self.phone_number + " )"

#############################
#       cityHistory
#############################
# 
# Fields:
#   user
#   location
#   time
#
#############################

class cityHistory(models.Model):
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE
    )

    location = models.ForeignKey(
        cityLocation,
        on_delete = models.CASCADE
    )

    time = models.DateTimeField(
        auto_now_add = True
    )

class cityTicket(models.Model):
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE
    )

    issue = models.TextField(
        default = "NONE",
        blank = True,
        null = True
    )

    completed = models.BooleanField(
        default = False
    )

    added = models.DateField(
        auto_now_add = True
    )

    edited = models.DateField(
        auto_now = True
    )



from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.generic import View, ListView, DetailView
from django.urls import reverse
from django.http import HttpResponse


import csv
from .forms import *
from .models import cityLocation, cityHistory, cityUser


class indexView(View):

    # Define templates, required forms and models required
    # to display this view
    template_name = "acc/index.html"

    loginForm = LoginUserForm
    registerForm = RegisterUserForm
    ticketForm = TicketUserForm
    # Use the index template to show the index page
    def get(self, request):

        context = {
            "loginForm" : self.loginForm(None), 
            "registerForm" : RegisterUserForm,
            "ticketForm" : self.ticketForm
        }
        return render(request, self.template_name, context)

class logoutView(View):

    # Logout the user and then redirect them to the index
    def get(self, request):
        logout(request)
        return redirect(reverse("index"))

################
## Debug view ##
################

class noView(View):

    def get(self,request):
        return redirect(reverse("index"))

class ticketView(View):

    ticketForm = TicketUserForm

    def post(self, request):
        
        form = self.ticketForm(request.POST)

        
        if form.is_valid():
            ticket = form.save(commit=False)

            issue = form.cleaned_data['issue']
            ticket.issue = issue
            ticket.user = request.user
            ticket.save()

        return redirect(reverse('index')) 



class locationSearchView(View):

    # Define templates, required forms and models required
    # to display this view
    context_object_name = "location_list"
    template_name = "location/locationSearch.html"

    loginForm = LoginUserForm
    registerForm = RegisterUserForm
    ticketForm = TicketUserForm

    # Display the location objects based on the occupation given in the URL
    def get(self, request, sType = None, sCity = None):

        if sCity == "None":
            sCity = "ALL"

        locations = cityLocation.objects.all()

        # If the page is navigated to and their is no occupation in the URL
        # redirect them to this page where the chosen URL relates to their default
        # occupation
        if request.user.is_authenticated() and sType == None:
            cUser = cityUser.objects.get(user = request.user)

            if cUser.occupation == "ST":
                return redirect(reverse("locationSearch", args = ['ST']))
            elif cUser.occupation == "BN":
                return redirect(reverse("locationSearch", args = ["BN"]))
            else:
                return redirect(reverse("locationSearch", args = ["TR"]))
        elif sType == None:
            return redirect(reverse("locationSearch", args = ["ALL"]))
        
        elif sType != None:
            if sType == "ST":
                locations = cityLocation.objects.filter(location_type = "CL") | cityLocation.objects.filter(location_type = "LB") 
            elif sType == "BN":
                locations = cityLocation.objects.filter(location_type = "HT") | cityLocation.objects.filter(location_type = "ID") 
            elif sType == "TR":
                locations = cityLocation.objects.filter(location_type = "HT")
            else:
                locations = cityLocation.objects.all()

        if sCity != None and sCity != "ALL":
            locations = locations.filter(city = sCity)

        # Pass context and display page
        context = {
            'location_list' : locations,
            'sType' : sType,
            'sCity' : sCity,
            "loginForm" : self.loginForm(None), 
            "registerForm" : self.registerForm,
            'ticketForm': self.ticketForm
        }

        return render(request, self.template_name, context)

class exportUserView(View):

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="userDatabase.csv"'

        writer = csv.writer(response)
        users = cityUser.objects.all()

        for cUser in users:
            writer.writerow([cUser.user.username, cUser.user.email, cUser.user.first_name, cUser.user.last_name, cUser.user.is_staff, cUser.phone_number, cUser.occupation, cUser.language])


        return response
        
class exportLocationView(View):

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="locationDatabse.csv"'

        writer = csv.writer(response)
        locations = cityLocation.objects.all()

        for location in locations:
            writer.writerow([location.name, location.address, location.phone_number, location.location_type, location.city, location.industry_type, location.departments, location.slug])
        return response

class userSearchView(View):

    # Define templates, required forms and models required
    # to display this view
    template_name = "user/userSearch.html"

    loginForm = LoginUserForm
    registerForm = RegisterUserForm
    ticketForm = TicketUserForm

    # Get all users and filter them based on the given username in the URL
    # if no username is given to compare to, return all users
    def get(self, request, searchUsername = None):

        users = User.objects.all()

        if searchUsername != None:
            
            users = User.objects.filter(username__contains = searchUsername)

        context = {
            'user_list' : users,
            "loginForm" : self.loginForm(None), 
            "registerForm" : RegisterUserForm,
            'ticketForm': self.ticketForm

            }

        return render(request, self.template_name, context)
    
    # Workaround to pass information from the form
    def post(self, request, searchUsername = None):

        username = request.POST['username']

        if username == '':
            return redirect(reverse("userSearch"))

        return redirect(reverse("userSearch", args=[username]))



class loginView(View):

    #Define templates, required forms and models required
    # to display this view

    form_class = LoginUserForm
    template_name = "acc/index.html"

    # As this page is used purely to process information, 
    # redirect to index if browesed to
    def get(self, request):
        return redirect(reverse("index"))

    # Grab username and password, check to see if a user can
    # be authenticated, if so, log them in.
    def post(self,request):
        
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username = username, password = password)

        if user is not None:
            login(request, user)
            return redirect(reverse("index"))

        return redirect(reverse("index"))


class registerView(View):

    #Define templates, required forms and models required
    # to display this view
    form_class = RegisterUserForm
    template_name = "acc/userForm.html"

    # As this page is used purely to process information, 
    # redirect to index if browesed to
    def get(self, request):
        return redirect(reverse("index"))
    
    # Check to see if the form is valid, if so
    # clean all data and pass it into a cityUser, and user model
    # if both are valid save and log the user in. After, redirect to index
    def post(self,request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user.username = username
            user.set_password(password)
            user.save()

            cUser = cityUser.objects.get(user = user)
            cUser.occupation = form.cleaned_data['occupation']
            cUser.occupation = form.cleaned_data['phone_number']
            cUser.save()

            user = authenticate(request, username = username, password = password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(reverse("index"))
            
        return redirect(reverse("index"))

class locationView(View):
    
    # Define templates, required forms and models required
    # to display this view
    template_name = "location/locationArticle.html"

    model = cityLocation
    context_object_name = "location"

    loginForm = LoginUserForm
    registerForm = RegisterUserForm
    ticketForm = TicketUserForm

    # By grabbig the slug from the URL, the relevent location model
    # can be selected. If it does not 404 the page.
    # If the user is logged in, save the current location to their history
    def get(self, request, slug):

        slocation = get_object_or_404(self.model, slug = slug)
        context = {
            'location' : slocation,
            "loginForm" : self.loginForm(None), 
            "registerForm" : RegisterUserForm,
            'ticketForm': self.ticketForm
            }

        if request.user.is_authenticated():
            history = cityHistory(user = request.user, location = slocation)
            history.save()


        return render(request, self.template_name, context)


class userView(View):

    # Define templates, required forms and models required
    # to display this view
    template_name = "user/userArticle.html"

    loginForm = LoginUserForm
    registerForm = RegisterUserForm
    ticketForm = TicketUserForm
    
    # Grab a user from the username given in the URL, if not
    # user can be found return a 404, if not pass the user into
    # the context and display the page
    def get(self,request, searchUsername):
        searchUser = get_object_or_404(User, username = searchUsername)

        locationHistorySet = cityHistory.objects.filter(user = searchUser).order_by('time')[:5]
        context = {
            'searchUser' : searchUser,
            'locationHistorySet' : locationHistorySet,
            "loginForm" : self.loginForm(None), 
            "registerForm" : RegisterUserForm,
            'ticketForm': self.ticketForm

        }

        return render(request, self.template_name, context)

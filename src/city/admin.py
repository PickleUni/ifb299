from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import cityUser, cityLocation, cityHistory, cityTicket
# Register your models here.

class cityLocationAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_filter  = ('location_type', "city")

class cityHistoryAdmin(admin.ModelAdmin):
    list_display = ('user', 'location', 'time')
    list_filter  = ('location','user')

class cityUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'occupation', 'language')
    list_filter  = ('occupation','language')

class cityTicketAdmin(admin.ModelAdmin):
    list_display = ('pk', 'issue','user', 'completed', 'edited', 'added')
    list_filter  = ('completed','added','edited')

admin.site.register(cityUser, cityUserAdmin)
admin.site.register(cityLocation,cityLocationAdmin)
admin.site.register(cityHistory, cityHistoryAdmin)
admin.site.register(cityTicket, cityTicketAdmin)
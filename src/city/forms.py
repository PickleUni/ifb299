from django.contrib.auth.models import User
from django import forms
from django.core.validators import MinLengthValidator
from .models import cityTicket

class TicketUserForm(forms.ModelForm):
    issue = forms.CharField(
        widget = forms.TextInput(attrs = 
            {
                "class": "input-text",
                "placeholder": "What is your issue?"
            }
        )
    )

    class Meta:
        model = cityTicket
        fields = ['issue']
    

class RegisterUserForm(forms.ModelForm):

    fType = "register"

    password = forms.CharField(
        widget = forms.PasswordInput(attrs = 
            {
                "class": "input-text",
                "placeholder": "Password"
            }
        )
    )

    username = forms.CharField(
        widget = forms.TextInput(attrs = 
            {
                "class": "input-text",
                "placeholder": "Username"
            }
        )
    )

    email = forms.CharField(
        required = False,
        widget = forms.EmailInput(attrs = 
            {
                "class": "input-text",
                "placeholder": "Email"
            }
        )
    ) 

    phone_number = forms.CharField(
        max_length = 10, 
        validators = [MinLengthValidator(8)],
        required = False,
        widget = forms.TextInput(attrs = 
            {
                "class": "input-select",
                "placeholder": "Phone Number",
            }
        )
    )

    occupation_choices = (
        ('ST', '\tStudent'),
        ('BN', '\tBusiness'),
        ('TR', '\tTourist')
    )

    occupation = forms.ChoiceField(
        widget=forms.Select(attrs=
            {
                "class": "input-text",
                "placeholder": "Occupation",
            }
        ),
        choices=occupation_choices
    )

    language_choices = {
        ('EN', '\tEnglish'),
        ('US', '\tAmerican English')
    }

    language = forms.ChoiceField(
        widget=forms.Select(attrs=
            {
                "class": "input-text",
                "placeholder": "Language",
                "default": "EN"
            }
        ),
        choices=language_choices
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'password'] 

class LoginUserForm(forms.ModelForm):

    fType = "login"

    password = forms.CharField(
        widget = forms.PasswordInput(attrs = 
            {
                "class": "input-text",
                "placeholder": "Password"
            }
        )
    )

    username = forms.CharField( 
        max_length = 10, 
        validators = [MinLengthValidator(8)],
        widget = forms.TextInput(attrs = 
            {
                "class": "input-text",
                "placeholder": "Username"
            }
        )
    )

    class Meta: 
        model = User
        fields = ['username', 'password']
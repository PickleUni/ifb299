# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-20 07:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('city', '0004_auto_20170920_1412'),
    ]

    operations = [
        migrations.AddField(
            model_name='citylocation',
            name='slug',
            field=models.SlugField(default='CHANGE-ME', max_length=64, unique=True),
        ),
    ]

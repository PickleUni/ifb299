# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-03 08:14
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('city', '0006_auto_20171002_1219'),
    ]

    operations = [
        migrations.AddField(
            model_name='cityuser',
            name='phone_number',
            field=models.CharField(default='0411122233', max_length=10, validators=[django.core.validators.MinLengthValidator(8)]),
        ),
    ]
